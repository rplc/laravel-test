<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    /**
     * Get the department the employe belongs to.
     */
    public function department() {
        return $this->belongsTo('App\Department');
    }
    /*relation setzen:
      $employe = App\Employe::find(...);
      $department = App\Department::find(...);
      $department->employes()->save($employe);
    */
}
