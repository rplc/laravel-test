<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employe;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller {

  public function index() {
    return view('dep.list', ['deps' => Department::all()]);
  }

  public function show($id) {
    return view('dep.single', ['dep' => Department::find($id)]);
  }

  public function addEmp($id) {
    return view('dep.addEmp', ['dep' => Department::find($id), 'emps' => Employe::doesntHave('department')->get()]);
  }

  public function storeEmp($id, $userId) {
    $emp = Employe::find($userId);
    $dep = Department::find($id);

    if ($emp && $dep) {
      $emp->department()->associate($dep);
      $emp->save();
    }

    return redirect()->route('depAddEmp', ['id' => $dep->id]);;
  }
}
