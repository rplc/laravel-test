<?php

namespace App\Http\Controllers;

use App\Employe;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;

class EmployeController extends Controller {

  public function index() {
    return view('emp.list', ['emps' => Employe::all()]);
  }

  public function store(Request $request) {
    $this->validate($request, [
      'firstname' => 'required|alpha',
      'lastname' => 'required|alpha',
      'email' => 'required|email'
    ]);

    //die('whatever');

    /*if ($validator->fails()) {
      return redirect()->route('empList')
        ->withErrors($validator)
        ->withInput();
    } else {*/
      $emp = new Employe;

      $emp->firstname = $request->firstname;
      $emp->lastname = $request->lastname;
      $emp->email = $request->email;

      $emp->save();

      return redirect()->route('empList');
    //}
  }
}
