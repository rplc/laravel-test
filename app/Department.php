<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    /**
    * Get the employes of this department
    */
    public function employes() {
        return $this->hasMany('App\Employe');
    }
}
