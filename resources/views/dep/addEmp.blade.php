@extends('layouts.app')

@section('title')
  Department {{ $dep->name }}
@endsection

@section('content')
  Name: <b>{{ $dep->name }}</b><br />

  Mitarbeiter in Abteilung:
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Vorname</th>
        <th>Nachname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($dep->employes as $emp)
        <tr>
          <td>{{ $emp->firstname }}</td>
          <td>{{ $emp->lastname }}</td>
          <td>{{ $emp->email }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

  Mitarbeiter ohne Abteilung:
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Vorname</th>
        <th>Nachname</th>
        <th>Email</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($emps as $emp)
        <tr>
          <td>{{ $emp->firstname }}</td>
          <td>{{ $emp->lastname }}</td>
          <td>{{ $emp->email }}</td>
          <td>
            <form action="{{ route('depStoreEmp', ['id' => $dep->id, 'userid' => $emp->id ]) }}" method="POST">
              {{ method_field('PUT') }}
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> </span>Mitarbeiter hinzufügen</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
