@extends('layouts.app')

@section('title', 'Alle Abteilungen')

@section('content')
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Anzahl Mitarbeiter</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($deps as $dep)
        <tr>
          <td>{{ $dep->name }}</td>
          <td>{{ count( $dep->employes ) }}</td>
          <td><a href="{{ route('depShow', ['id' => $dep->id]) }}"><i class="fa fa-info" aria-hidden="true"></i></a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
