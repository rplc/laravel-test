@extends('layouts.app')

@section('title')
  Department {{ $dep->name }}
@endsection

@section('content')
  Name: <b>{{ $dep->name }}</b><br />

  Mitarbeiter:
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Vorname</th>
        <th>Nachname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($dep->employes as $emp)
        <tr>
          <td>{{ $emp->firstname }}</td>
          <td>{{ $emp->lastname }}</td>
          <td>{{ $emp->email }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <a href="{{ route('depAddEmp', ['id' => $emp->department->id]) }}" class="btn btn-primary btn-lg" role="button" aria-pressed="true"><i class="fa fa-plus-square" aria-hidden="true"></i> Mitarbeiter hinzufügen</a>
@endsection
