@extends('layouts.app')

@section('title', 'Alle Mitarbeiter')

@section('content')
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Vorname</th>
        <th>Nachname</th>
        <th>Email</th>
        <th>Abteilung</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($emps as $emp)
        <tr>
          <td>{{ $emp->firstname }}</td>
          <td>{{ $emp->lastname }}</td>
          <td>{{ $emp->email }}</td>
          @if ($emp->department)
            <td><a href="{{ route('depShow', ['id' => $emp->department->id]) }}">{{ $emp->department->name }}</a></td>
          @else
            <td>Keiner Abteilung zugewiesen</td>
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>

  <br />
  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#newEmp">
    <i class="fa fa-plus-square" aria-hidden="true"></i> Neuer Mitarbeiter
  </button>

  <div class="modal fade" id="newEmp" tabindex="-1" role="dialog" aria-labelledby="newEmpLabel">
    <div class="modal-dialog" role="document">
      <form class="modal-content" action="{{ route('empStore') }}" method="POST">
        {{ method_field('PUT') }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="newEmpLabel">Neuer Mitarbeiter</h4>
        </div>
        <div class="modal-body">
          <div class="form-group @if ($errors->has('firstname')) has-error @endif">
            <label for="firstname" class="control-label">Vorname</label>
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Vorname" value="{!! old('firstname') !!}" aria-describedby="firstnameBox">
            @if ($errors->has('firstname'))
              <div id="firstnameBox" class="help-block">
                @foreach ($errors->get('firstname') as $message)
                  {{ $message }}<br />
                @endforeach
              </div>
            @endif
          </div>
          <div class="form-group @if ($errors->has('lastname')) has-error @endif">
            <label for="lastname" class="control-label">Nachname</label>
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Nachname" value="{!! old('lastname') !!}" aria-describedby="lastnameBox">
            @if ($errors->has('lastname'))
              <div id="lastnameBox" class="help-block">
                @foreach ($errors->get('lastname') as $message)
                  {{ $message }}<br />
                @endforeach
              </div>
            @endif
          </div>
          <div class="form-group @if ($errors->has('email')) has-error @endif">
            <label for="email" class="control-label">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{!! old('email') !!}" aria-describedby="emailBox">
            @if ($errors->has('email'))
              <div id="emailBox" class="help-block">
                @foreach ($errors->get('email') as $message)
                  {{ $message }}<br />
                @endforeach
              </div>
            @endif
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-plus-square" aria-hidden="true"></i> </span>Mitarbeiter anlegen</button>
        </div>
      </form>
    </div>
  </div>

  @if (count($errors) > 0)
    <script type="text/javascript">
      $('#newEmp').modal('show');
    </script>
  @endif
@endsection
