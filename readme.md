# Installation
- composer global require "laravel/installer"
  - im htdocs von xampp
- $HOME/.composer/vendor/bin (%AppData%\Composer\vendor\bin) in die Path Umgebungsvariable rein
- .env.example in .env umbenennen
  - db user eintragen, url anpassen
  - php artisan key:generate ausführen

## Editor
- **editorConfig** Extionsion installieren

## Nach Clonen
- composer install
- npm install (prüfen, dass node7 installiert ist)
- .env.example nach .env kopieren
  - prüfen dass die DB Verbindung passt (User, DB usw. angelegt)
  - ggf. App Key generieren (php artisan key:generate)
- php artisan migrate
- npm run dev (bzw. npm run watch)

## FYI
- [Build Process](https://laravel.com/docs/5.3/elixir)
- [Laroute](https://github.com/aaronlord/laroute)
- [Entrust](https://github.com/Zizaco/entrust)
- **php artisan tinker** mit allem rumspielen, auch auf Daten zugreifen und so

## Plan
- User Verwaltung (Abteilungen und so)
- Rollenmanagement mit rein nehmen
- nur eingeloggte Benutzer dürfen Person - Abteilungen Mapping pflegen
- nur eingeloggte mit entsprechender Rolle dürfen Abteilungen/Personen anlegen
- Tabelle, die Personen in Abteilungen anzeigt (mittels Ajax machen)
- optional: [Localization](https://laravel.com/docs/5.4/localization)

# Routing
- Host Datei: `127.0.0.1 laravel.local`
- httpd-vhost.conf
```
<VirtualHost *:80>
    DocumentRoot "C:\xampp\htdocs\laravel\public"
    ServerName laravel.local
    ServerAlias www.laravel.local
</VirtualHost>
```
