<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EmployeController@index')->name('empList');
Route::put('/emp/store', 'EmployeController@store')->name('empStore');

Route::put('/dep/{id}/add/{userid}', 'DepartmentController@storeEmp')->name('depStoreEmp');
Route::get('/dep/{id}/add', 'DepartmentController@addEmp')->name('depAddEmp');
Route::get('/dep/{id}', 'DepartmentController@show')->name('depShow');
Route::get('/dep/', 'DepartmentController@index')->name('depList');
